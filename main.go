package main

import (
	"time"

	"flag"

	"github.com/go-vgo/robotgo"
	hook "github.com/robotn/gohook"
)

func shakeMouse() {
	robotgo.MoveSmoothRelative(100, 0, 1.0, 1.0)
	robotgo.MoveSmoothRelative(-200, 0, 1.0, 1.0)
	robotgo.MoveSmoothRelative(100, 0, 1.0, 1.0)
}

func shakeWhenIdle(lull time.Duration, movementUpdated chan time.Time) {
	lastUpdate := time.Now()

	go func() {
		for crtTime := range movementUpdated {
			lastUpdate = crtTime
		}
	}()

	wait := lull
	for {
		time.Sleep(wait)
		lastMovement := time.Now().Sub(lastUpdate)
		if lastMovement > lull {
			shakeMouse()
			wait = lull
		} else {
			wait = lull - lastMovement
		}
	}
}

func main() {

	var lullTimeFlag int
	flag.IntVar(&lullTimeFlag, "idle-minutes", 5, "How many minutes to wait before shaking mouse")
	flag.Parse()

	shakeMouse()

	lullDuration := time.Minute * time.Duration(lullTimeFlag)
	timestamps := make(chan time.Time)

	hook.Register(hook.MouseMove, []string{}, func(e hook.Event) {
		timestamps <- time.Now()
	})

	go shakeWhenIdle(lullDuration, timestamps)

	s := hook.Start()
	<-hook.Process(s)

}
