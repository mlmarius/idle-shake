# idle-shake

Shake the mouse when the computer has been idling for too long in order to prevent you from becoming idle on Teams and
whatnot.